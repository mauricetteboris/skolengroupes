# Skolengroupes

En attendant que l'éditeur de Skolengo offre la possibilité de récupérer directement les groupes dans le fichier d'export, vous pouvez utiliser l'application Skolengroupes pour générer un fichier compilant les informations de Skolengo et de Pronote

## Générer le fichier ENT

Aller dans Annuaire -> administration -> export CSV spécifique:  
sélectionner les champs **Nom**, **Prénom**, **Profil**, **Identifiant ENT**, **classe**  

![Selection ENT](groupes_elea_ent.png)

Enregistrer le fichier obtenu

## Générer le fichier EDT 

### Aller dans Pronote avec un compte autorisé à faire des exports.

### Aller dans élèves et sléectionner **les élèves affectés à une classe**
![Selection ENT](groupes_elea0.png)

### Aller dans Imports/Exports -> TXT/CSV/XML -> Exporter un fichier texte
![Selection ENT](groupes_elea1.png)

### Sélectionner:
- Types de données à exporter : Elèves et leurs responsables
- Format CSV
- Cliquer sur la clé anglaise et ne garder que les champs: Nom, Prénom, Classes, Groupes puis valider
- Indiquer comme séparateur de ressources multiples : **,**
- Exporter

![Selection ENT](groupes_elea2.png)


## Lancer le logiciel [Skolengroupes](Skolengroupes.exe)
- Sélectionner le fichier provenant de l'ENT
- Sélectionner le fichier provenant de Pronote (facultatif)
- Sélectionner le dossier de destination du résultat.

## Import dans Éléa
Avec le compte établissement, cliquer sur importer des utilisateurs puis déposer le fichier généré par Skolengroupes