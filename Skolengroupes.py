# coding: utf-8
import csv
from tkinter import Tk
from tkinter.filedialog import askopenfilename, askdirectory

Tk().withdraw()
ent_file = askopenfilename(title="Sélectionner le Fichier ENT")
nbAAF=''
if ent_file :
    with open(ent_file,'r', encoding='iso-8859-1') as ent_loaded :
        ent_csv=csv.DictReader(ent_loaded, delimiter=';')
        ent_data=[['Nom','Prénom','Profil','Identifiant ENT','classe','groupes']]
        for row in ent_csv:
            ent_data.append([row['Nom'],row['Prénom'],row['Profil'],row['Identifiant ENT'],row['classe'].upper(),''])

pronote_file = askopenfilename(title="Sélectionner le Fichier PRONOTE")
if pronote_file :
    erreurs=[]
    with open(pronote_file,'r', encoding='utf8') as pronote_loaded :
        pronote_csv=csv.DictReader(pronote_loaded, delimiter=';')
        pronote_data=[]
        for row in pronote_csv:
            pronote_data.append([row['\ufeffNOM'],row['PRENOM'],row['CLASSES'].upper(),row['GROUPES'].upper()])

for ent_line in ent_data:
    if nbAAF=='':
        if '~' in ent_line[4]:
            nbAAF=str(ent_line[4].split('~',)[0])
    if (ent_line[2]=='Eleve'):
        matched=False
        for pronote_line in pronote_data:
            if (ent_line[0]==pronote_line[0] and ent_line[1]==pronote_line[1] and ent_line[4]==nbAAF+'~'+pronote_line[2]):
                matched=True
                if (pronote_line[3] !=''):
                    ent_line[5]= nbAAF+'~'+pronote_line[3].replace(', ',','+nbAAF+'~')
        if not matched:
            erreurs.append([ent_line[0],ent_line[1],ent_line[5]])

ent_result=[]

for ent_line in ent_data:
    ent_result.append(';'.join(ent_line)+'\n')
    

receivePath = askdirectory(title="Dossier de destination")
with open(receivePath+'/erreurs.csv','w', newline='', encoding='utf8') as csvfile :
    spamwriter = csv.writer(csvfile, delimiter=';')
    for erreur in erreurs:
        spamwriter.writerow(erreur)

with open(receivePath+'/export.csv','w', encoding='iso-8859-1') as file :
    for compte in ent_result:
        file.write(compte)
    
WINDOWS_LINE_ENDING = b'\r\n'
UNIX_LINE_ENDING = b'\n'

with open(receivePath+'/export.csv', 'rb') as open_file:
    content = open_file.read()
    
content = content.replace(WINDOWS_LINE_ENDING, UNIX_LINE_ENDING)

with open(receivePath+'/export.csv', 'wb') as open_file:
    open_file.write(content)