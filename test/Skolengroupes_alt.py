import pandas as pd
import re
from tkinter import Tk
from tkinter.filedialog import askopenfilename, askdirectory
from os.path import join as opjoin
# alternative à Skolengroupes.py

Tk().withdraw()
ent_file = askopenfilename(title="Sélectionner le Fichier ENT")
pronote_file = askopenfilename(title="Sélectionner le Fichier PRONOTE")
receivePath = askdirectory(title="Dossier de destination")

pf = re.compile("^(\d+)~(.*)") # regexp pour capture préfixe AAF
champs = ['Nom','Prénom','Profil','Identifiant ENT','classe','groupes']

def pfAAF(d):
    """récupérer le préfixe AAF et la classe d'un champ classe"""
    look = pf.search(d)
    return look.groups() if look else ["",""]

# préparation données ENT: classe en majuscule, split de l'AAF
IN1 = pd.read_csv(ent_file, sep=';', header=0, encoding='iso-8859-1',
                  usecols=[0,1,2,3,4]).dropna(axis=0, subset="Identifiant ENT")
IN1.drop(IN1[~IN1["Profil"].isin(['Eleve', 'Professeur', 'Personnel'])].index, inplace=True)
IN1.sort_values(by=["classe", "Nom", "Prénom"], inplace=True)
cl = IN1.pop("classe").fillna(value="").apply(lambda e: e.upper())
IN1.insert(3, "classe", cl) # classes en upper()
A = pd.DataFrame(data=cl.apply(pfAAF).tolist(), columns=["AAF", "CLASSE"], index=IN1.index)
ENT = pd.concat([IN1, A], axis=1)
# id unique CLASSE + nom + prénom normalisés
pn = ENT['Prénom'].map(lambda e: e.capitalize(), na_action='ignore')
nom = ENT['Nom'].map(lambda e: e.upper(), na_action='ignore')
id = ENT["CLASSE"]+" "+nom+ " "+ pn
ENT.insert(0, "id", id)

# préparation données pronote
IN2 = pd.read_csv(pronote_file, sep=";",
                  header=0, encoding='utf8').dropna(axis=0, subset=["CLASSES"])
pn2 = IN2['PRENOM'].map(lambda e: e.capitalize(), na_action='ignore')
id_pn =IN2["CLASSES"]+" " + IN2['NOM']+ " " + pn2
IN2.insert(0, "id", id_pn)
IN2.sort_values(by="id", inplace=True)

# fusion
df = pd.merge(IN2[["id", "GROUPES", "CLASSES"]], ENT, on="id", how="outer", suffixes=('_ent', '_pn'))#.fillna(value="")
# réécriture des groupes avec préfixe AAF
gp = df[~df["GROUPES"].isna()].apply(lambda e: ','.join(map(lambda f: f"{e['AAF']}~{f.strip()}", e["GROUPES"].split(','))), axis=1)
df.insert(0, "groupes", gp)
# élèves ENT pas dans pronote: 
vides = df[df["CLASSES"].isna()]

#export final et erreurs
df[champs].to_csv(opjoin(receivePath, "exportFull.csv"), sep=";", encoding='iso-8859-1', index=False)
vides.to_csv(opjoin(receivePath, "erreurs.csv"), header=False, encoding='utf8')
